/*global $*/
(function(window){
    
    function onMouseEnterLeave(){
        function action(e){
            if(e.type == 'mouseenter'){
                $(this).addClass('open');
            }else{
                $(this).removeClass('open');
            }
        }
        $(document).on({mouseenter:action,mouseleave:action},'.navbar .dropdown.items-category');
    }
    function onItemThumbClick(){
        function action(){
            var link = $(this).find('a');
            window.location = link.attr('href');
        }
        $(document).on("click",'.item-thumb',action);
    }
    
    function onDailySpecClick(){
        function action(){
            var href = $(this).data('href');
            window.open(href, '_blank');
        }
        $(document).on('click','.daily_spec',action);
    }
    
    function itemsPerPage(){
        function action(){
            var select = $(this);
            var url = window.location.href.toString();
            if(url.lastIndexOf('?limit') != -1){
                url = url.substring(url.lastIndexOf('?limit'),-1);
            }   
            window.location.replace(url+'?'+select.val());
        }
        $(document).on('change','#items_per_page',action);
    }
    var map;
    var marker;
    function initMap() {
    if(document.getElementById('map')){
        var mapElement = document.getElementById('map');
        var latLng = {lat:Number(mapElement.dataset.lat), lng: Number(mapElement.dataset.lng)};
        map = new google.maps.Map(mapElement, {
        center: latLng,
        zoom:Number(mapElement.dataset.zoom)
      });
        var marker = new google.maps.Marker({
            position: latLng,
            map: map,
            title: mapElement.dataset.addr
          });
    }

    }
    $(document).ready(function(){
         $(".main-slider").owlCarousel({
            slideSpeed : 300,
            paginationSpeed : 400,
            singleItem: true,
    	    pagination: false,
        	rewindSpeed: 500
    	});
      $(".owl-carousel").owlCarousel({
            margin:15,
            autoplay:false,
            loop:true,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:3
                },
                1000:{
                    items:3
                }
            }
      });
    // var mp4Link = $(".bv-video").data('link');
    // const backgroundVideo = new BackgroundVideo('.bv-video', {
    //   src: [
    //     mp4Link
    //   ],
    //   onReady: function () {
    //     // Use onReady() to prevent flickers or force loading state
    //     const vidParent = document.querySelector(`.${this.bvVideoWrapClass}`);
    //     vidParent.classList.add('bv-video-wrap--ready')
    //     vidParent.style.height = 150;
    //     vidParent.style.width = 150;
    //   }
    // });
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    });
    onItemThumbClick();
    onMouseEnterLeave();
    itemsPerPage();
    onDailySpecClick();
    initMap();
})(window)