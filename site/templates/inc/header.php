<!DOCTYPE html>
<?php 
function nested2ul($data,$level) {
  $result = array();
  if (sizeof($data) > 0) {
    $class = $level == 0 ? 'nav navbar-nav navbar-left':'dropdown-menu';
    $result[] = '<ul class="'.$class.'">';
    foreach ($data as $entry) {
        $level++;
        $link;
            if($entry->hasChildren("template=subcategory") && $entry->parent->id != $entry->parents->eq(0)->id){
                 $cl = 'dropdown-submenu'; 
                 $link = '#';
            }elseif($entry->hasChildren("template=subcategory") && $entry->parent->id == $entry->parents->eq(0)->id){
                $cl = 'dropdown items-category';
                $link = '#';
            }else{
                $cl = ''; 
                $link = $entry->url;
            }
    		$result[] = sprintf('<li class="'.$cl.'"><a href="%s">%s</a> %s</li>',$link,$entry->title,nested2ul($entry->children("template=subcategory"),$level));
    }
    $result[] = '</ul>';
  }
  return implode($result);
}
?>
<html class="full" lang="<?php echo $homepage->getLanguageValue($user->language, 'name')?>">
<!-- Make sure the <html> tag is set to the .full CSS class. Change the background image in the full.css file. -->

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $page->title.' - '.$pages->get('/site-settings/')->site_name?></title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo  $config->urls->templates."libs/bootstrap-3.3.7-dist/css/bootstrap.min.css"; ?>">

    <link href="<?php echo $config->urls->templates."libs/owl.carousel.2.0.0-beta.2.4/assets/owl.carousel.css"?>" rel="stylesheet">

    <link href="<?php echo $config->urls->templates."libs/fotorama-4.6.4/fotorama.css"?>" rel="stylesheet">

    <!--<link href="<?//php echo $config->urls->templates?>libs/backgroundVideo/css/normalize.css" rel="stylesheet">-->


    <!--<link href="<?//php echo $config->urls->templates?>libs/backgroundVideo/css/styles.css" rel="stylesheet">-->

    <!-- Custom CSS -->
    <link href="<?php echo $config->urls->templates."styles/main.css"?>" rel="stylesheet">
                	<?php
                	foreach($languages as $language) {
                		// if this page is not viewable in the language, skip it
                		if(!$page->viewable($language)) continue;
                		// get the http URL for this page in the given language
                		$url = $page->localHttpUrl($language); 
                		// hreflang code for language uses language name from homepage
                		$hreflang = $homepage->getLanguageValue($language, 'name'); 
                		// output the <link> tag: note that this assumes your language names are the same as required by hreflang. 
                		echo "\n\t<link rel='alternate' hreflang='$hreflang' href='$url' />";
                	}
                	
                	?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top card-2" role="navigation">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/<?php echo $homepage->getLanguageValue($user->language, 'name')?>">
                    <img src="<?php echo $pages->get('/site-settings/')->logo->url?>">
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <?php echo nested2ul($homepage->and($homepage->children),0);?>
                    <?php if($page->editable()) echo "<ul class='nav navbar-nav  navbar-right'><li class='edit'><a href='$page->editUrl'>Edit</a></li></ul>";
                	?>
                	
                	

                <ul class="nav navbar-nav navbar-right">
                                    	<li class="dropdown">
                      <a href="#" class="dropdown-toggle text-uppercase" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $homepage->getLanguageValue($user->language, 'name')?> <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                         <?php foreach($languages as $language):?>
                		    <?php if(!$page->viewable($language)) continue;
                			if($language->id == $user->language->id) {
                				echo "<li class='active'>";
                			} else {
                				echo "<li>";
                			}
                			$url = $page->localUrl($language); 
                			$hreflang = $homepage->getLanguageValue($language, 'name'); 
                			echo "<a hreflang='$hreflang' href='$url'>$language->title</a></li>";?>
	                    <?php endforeach;?>
                      </ul>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>