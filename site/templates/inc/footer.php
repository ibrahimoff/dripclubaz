<footer class="footer">
      <div class="container">
        <p class="text-muted">Place sticky footer content here.</p>
      </div>
    </footer>
    <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $pages->get('/site-settings/')->google_maps_key?>"></script>

    <!-- jQuery -->
    <script src="<?php echo $config->urls->templates."libs/jquery/jquery-1.9.1.js"?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo $config->urls->templates."libs/bootstrap-3.3.7-dist/js/bootstrap.min.js"?>"></script>
    
    <script src="<?php echo $config->urls->templates."libs/owl.carousel.2.0.0-beta.2.4/owl.carousel.min.js"?>"></script>

    <script src="<?php echo $config->urls->templates."libs/fotorama-4.6.4/fotorama.js"?>"></script>
    
    <script src="<?php echo $config->urls->templates."libs/parallax.js-1.4.2/parallax.js"?>"></script>
    <!--<script src="<?//php echo $config->urls->templates;?>libs/backgroundVideo/dist/backgroundVideo.js"></script>-->

    <script src="<?php echo $config->urls->templates."scripts/main.js"?>"></script>

</body>
</html>
