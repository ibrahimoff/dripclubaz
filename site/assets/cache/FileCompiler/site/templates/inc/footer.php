<footer class="footer">
      <div class="container">
        <p class="text-muted">Place sticky footer content here.</p>
      </div>
    </footer>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=<?php echo $pages->get('/site-settings/')->google_maps_key?>"></script>

    <!-- jQuery -->
    <script src="<?php echo AIOM::JS("libs/jquery/jquery-1.9.1.js")?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo AIOM::JS("libs/bootstrap-3.3.7-dist/js/bootstrap.min.js")?>"></script>
    
    <script src="<?php echo AIOM::JS("libs/owl.carousel.2.0.0-beta.2.4/owl.carousel.min.js")?>"></script>

    <script src="<?php echo AIOM::JS("libs/fotorama-4.6.4/fotorama.js")?>"></script>

    <!--<script src="<?//php echo $config->urls->templates;?>libs/backgroundVideo/dist/backgroundVideo.js"></script>-->

    <script src="<?php echo AIOM::JS("scripts/main.js")?>"></script>

</body>
</html>
