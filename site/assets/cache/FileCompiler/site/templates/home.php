<?php namespace ProcessWire;
require('./inc/header.php'); ?>
<?php /** @var Object $page */
if (!$page->image_slider):?>
    <div class="main-slider">
        <div class="owl-slide"
             style="background-image: url('https://images.unsplash.com/photo-1437915015400-137312b61975?fit=crop&fm=jpg&h=800&q=80&w=1200');"></div>
    </div>
<?php else: ?>
    <section class="hero hidden-xs">
        <video id="home-background" autoplay muted loop>
            <source src="<?php echo $page->main_video->url ?>" type="video/mp4">
            Your browser does not support the video tag.
        </video>
    </section>
<?php endif; ?>

    <!--<main>-->
    <!--  <video muted class="bv-video" data-link="<? //php echo $page->main_video->url
    ?>"></video>-->
    <!--</main>-->

    <div class="content">
        <div class="container">
            <div class="daily-special-block">
                <div class="section-header"><span><?php echo 'Daily special'?></span></div>

                <?php $results = $pages->find('template=post,limit=1,sort=-id');
                if ($results):
                    foreach ($results as $value):?>
                        <div class="well daily_spec card-2" data-href="<?php echo $value->link ?>">
                            <div class="row">
                                <div class="col-md-6">
                                    <img class="img-responsive" src="<?php echo $value->picture->url ?>" alt="">
                                </div>
                                <div class="col-md-6">
                                    <div class="well-body">
                                        <h4 class="media-heading"><?php echo $value->title ?></h4>
                                        <p><?php echo $value->body ?></p>
                                        <div class="read_more pull-left"><a href="<?php echo $value->link ?>">Read more</a></div>

                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                        </div>

                    <?php endforeach;endif; ?>
            </div>

<!--            <div class="well daily_spec card-2" data-href="--><?php //echo $value->link ?><!--">-->
<!--                <div class="media post-block">-->
<!--                    <div class="media-left hidden-xs  hidden-sm">-->
<!--                        <img class="media-object" src="--><?php //echo $value->picture->url ?><!--" alt="">-->
<!--                    </div>-->
<!--                    <div class="media-body">-->
<!--                        <h4 class="media-heading">--><?php //echo $value->title ?><!--</h4>-->
<!--                        <p>--><?php //echo $value->body ?><!--</p>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
            <?php
            $curr = $pages->get('/site-settings/')->currency;
            foreach ($page->sliders as $slider):?>
                <div class="section-header"><span><?php echo $slider->headline ?></span></div>
                <div class="owl-carousel main-carousel">
                    <?php foreach ($slider->selected_items as $value): ?>
                        <?php echo renderItemThumb($value, $curr) ?>
                    <?php endforeach; ?>


                </div>
            <?php endforeach; ?>


        </div>
    </div>
    <div class="content">
        <!--<div class="section-bgr" style="background:url(/site/templates/test.jpg);"></div>-->
        <div class="parallax-window hidden-xs hidden-sm" data-parallax="scroll" data-image-src="/site/templates/test.jpg"></div>
        <div class="black-overlay"></div>
        <div class="container contacts-section">
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <div class="address-block">
                        <div class="sh1">CONTACT INFO</div>
                        <ul>
                            <li class="sh2">Phone: 262 695 2601</li>
                            <li class="sh2">1967 Fairfield Road Pewaukee, WI 53072</li>
                            <li class="sh2">support@octopus-theme.com</li>
                        </ul>
                        <div class="sh1">GET IN TOUCH</div>
                        <ul>
                            <li>fb</li>
                            <li>tw</li>
                            <li>vk</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-xs-12">


                    <form class="form-horizontal no-margin-form-group">
                        <fieldset>

                            <!-- Text input-->

                            <div class="form-group">
                                <div class="col-md-6">
                                    <div class="group">
                                        <input type="text" required>
                                        <span class="bar"></span>
                                        <label>First name</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="group">
                                        <input type="text" required>
                                        <span class="bar"></span>
                                        <label>Last name</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="group">
                                        <input type="text" required>
                                        <span class="bar"></span>
                                        <label>Email</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="group">
                                        <input type="text" required>
                                        <span class="bar"></span>
                                        <label>Subject</label>
                                    </div>
                                </div>
                            </div>
                            <!-- Textarea -->
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="group">
                                        <textarea id="textarea" name="message" required></textarea>
                                        <span class="bar"></span>
                                        <label>Message</label>
                                    </div>
                                </div>
                            </div>

                            <!-- Button -->
                            <div class="form-group">
                                <div class="col-md-12">
                                    <button class="btn btn-block" type="button"><span>Button</span></button>
                                </div>
                            </div>

                        </fieldset>
                    </form>
                </div>
            </div>
        </div>

    </div>
    <div id="map" style="height:300px;" data-addr="<?php echo $page->google_map->address ?>"
         data-lat="<?php echo $page->google_map->lat ?>" data-lng="<?php echo $page->google_map->lng ?>"
         data-zoom="<?php echo $page->google_map->zoom ?>"></div>
<?php require('./inc/footer.php'); ?>