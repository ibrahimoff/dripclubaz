<?php namespace ProcessWire;
require('./inc/header.php');?>
<div class="content"> 
    <div class="container">
        <div class="row">

            <div class="col-lg-12"> <h1><?php echo $page->title?></h1></div>    
            <div class="col-lg-12"><?php echo $page->body?></div>     
        </div>
       
    </div>    
</div>        


<?php require('./inc/footer.php');?>